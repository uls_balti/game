var io = require("socket.io")(3000);

io.on('connection', function(socket) {
    console.log("Connected socket - " + socket.id);
    
    var listOfSockets = Object.keys(io.sockets.connected);
    
    socket.emit('all-players-list', {
        my_id: socket.id,
        list: listOfSockets
    });
    socket.broadcast.emit('new-player', socket.id);
    
    
    
    socket.on('move', function(data) {
        socket.broadcast.emit('move', {
            player: socket.id,
            coords: data
        });
    });
    
    socket.on('disconnect', function() {
        socket.broadcast.emit('player-disconnect', socket.id);
        console.log('Disconnected socket - ' + socket.id);
    });
    
});