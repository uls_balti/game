function Game() {
    var self = this;
    
    self.gameField = $('#game-field');
    self.players = {};
    self.myID = null;
    self.socket = io("http://127.0.0.1:3000");
    
    
    self.socket.on('all-players-list', function(data) {
        self.myID = data.my_id;
        
        for(var i = 0; i < data.list.length; i++) {
            self.addPlayer(data.list[i], new Player());    
        }
    });
    
    self.socket.on('new-player', function(data) {
        self.addPlayer(data, new Player());    
    });
    
    self.socket.on('move', function(data) {
        self.players[data.player].move(data.coords);
    });
    
    self.socket.on('player-disconnect', function(data) {
        self.players[data].remove();
    });
    

    
    
    self.initListeners();
}

Game.prototype.addPlayer = function(ID, player) {
    this.players[ID] = player;
    this.gameField.append(player.getPlayer(ID));
}

Game.prototype.initListeners = function() {
    
    this.gameField.on('click', this.fieldClick());
    
}

Game.prototype.fieldClick = function() {
    var self = this;
    return function(event) {
        var coords = {
            x: event.offsetX,
            y: event.offsetY
        }
        self.socket.emit('move', coords);
        self.setTargetMarker(coords);
        self.players[self.myID].move(coords, function(){
            $('#marker').remove();
        });
    }
}



Game.prototype.setTargetMarker = function(coords) {
    if(this.gameField.find('#marker').size() == 0) {
        this.gameField.append('<div id="marker" class="marker"></div>');
    }
    
    $('#marker').
        css('left', (coords.x - 5) + 'px').
        css('top', (coords.y - 5) + 'px');
}



var p = new Game();
