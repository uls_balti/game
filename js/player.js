function Player() {
    this.defaultPlayerRadius = 30;
    this.defaultPlayerColor = '#086f88';
    this.playerSpeed = 500; // px/sec
    this.player = null;
    
}

Player.prototype.getPlayer = function(ID) {
    this.player = $('<div id="'+ID+'"></div>').addClass('player').css({
        'width': (this.defaultPlayerRadius * 2) + 'px',
        'height': (this.defaultPlayerRadius * 2) + 'px',
        'background': this.defaultPlayerColor
    });
    return this.player;
}

Player.prototype.remove = function(ID) {
    this.player.remove();
}

Player.prototype.move = function(coords, callback) {
    var xdist = Math.abs(coords.x - this.player.position().left);
    var ydist = Math.abs(coords.y - this.player.position().top);
    var dist = Math.ceil(Math.sqrt(Math.pow(xdist, 2) + Math.pow(ydist, 2)));
    
    var time = dist / this.playerSpeed; // in sec
    
    this.player.stop();
    this.player.animate({
        'left': (coords.x - this.defaultPlayerRadius) + 'px',
        'top': (coords.y - this.defaultPlayerRadius) + 'px'
    }, time * 1000, "linear", function() {
        callback && callback();
    });
}